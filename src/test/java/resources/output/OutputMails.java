package resources.output;

public class OutputMails {
    public static final String VERIFY_UNVERIFIED_VALUE = "output/mails/verify_unverified.json";
    public static final String FORGOT_PASSWORD_VERIFIED_VALUE = "output/mails/forgot_password_verified.json";
}
