package resources.input;

public class InputUsers {
    public static final String UNVERIFIED_KEY = "input/users/unverified.key.json";
    public static final String UNVERIFIED_VALUE = "input/users/unverified.json";
    public static final String UNVERIFIED_DELETE_VALUE = "input/users/unverified_delete.json";

    public static final String VERIFIED_KEY = "input/users/verified.key.json";
    public static final String VERIFIED_VALUE = "input/users/verified.json";
    public static final String VERIFIED_DELETE_VALUE = "input/users/verified_delete.json";
}
