package resources.input;

public class InputForgotPasswordTokens {
    public static final String VERIFIED_KEY = "input/forgotPasswordTokens/verified.key.json";
    public static final String VERIFIED_VALUE = "input/forgotPasswordTokens/verified.json";
    public static final String VERIFIED_DELETE_VALUE = "input/forgotPasswordTokens/verified_delete.json";
}
