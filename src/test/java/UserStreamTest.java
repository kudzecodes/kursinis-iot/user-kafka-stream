import lt.kudze.auth.UserStream;
import lt.kudze.auth.topics.DebeziumForgotPasswordTokensTopic;
import lt.kudze.auth.topics.DebeziumUsersTopic;
import lt.kudze.auth.topics.MailerTopic;
import lt.kudze.auth.topics.VerifiedUsersTopic;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TestInputTopic;
import org.apache.kafka.streams.TestOutputTopic;
import org.apache.kafka.streams.TopologyTestDriver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import resources.input.InputForgotPasswordTokens;
import resources.input.InputUsers;
import resources.output.OutputMails;
import resources.output.OutputUsers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;

public class UserStreamTest {

    public void testThatForgotPasswordTokenDeleteDoesNothing(
            TestInputTopic<String, String> inputForgotPasswordTokenTopic,
            TestOutputTopic<String, String> outputVerifiedUsersTopic,
            TestOutputTopic<String, String> outputMailerTopic
    ) throws IOException {
        inputForgotPasswordTokenTopic.pipeInput(
                getResource(InputForgotPasswordTokens.VERIFIED_KEY),
                getResource(InputForgotPasswordTokens.VERIFIED_DELETE_VALUE)
        );

        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());
        Assertions.assertTrue(outputMailerTopic.isEmpty());
    }

    public void testThatForgotPasswordTokenTombstoneDoesNothing(
            TestInputTopic<String, String> inputForgotPasswordTokenTopic,
            TestOutputTopic<String, String> outputVerifiedUsersTopic,
            TestOutputTopic<String, String> outputMailerTopic
    ) throws IOException {
        inputForgotPasswordTokenTopic.pipeInput(
                getResource(InputForgotPasswordTokens.VERIFIED_KEY),
                "null"
        );

        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());
        Assertions.assertTrue(outputMailerTopic.isEmpty());
    }

    public void testThatForgotPasswordTokenOperationsDoesNothing(
            TestInputTopic<String, String> inputForgotPasswordTokenTopic,
            TestOutputTopic<String, String> outputVerifiedUsersTopic,
            TestOutputTopic<String, String> outputMailerTopic
    ) throws IOException {
        pipeForgotPasswordVerifiedToken(inputForgotPasswordTokenTopic);

        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());
        Assertions.assertTrue(outputMailerTopic.isEmpty());

        testThatForgotPasswordTokenDeleteDoesNothing(
                inputForgotPasswordTokenTopic,
                outputVerifiedUsersTopic,
                outputMailerTopic
        );

        testThatForgotPasswordTokenTombstoneDoesNothing(
                inputForgotPasswordTokenTopic,
                outputVerifiedUsersTopic,
                outputMailerTopic
        );
    }

    public void pipeForgotPasswordVerifiedToken(TestInputTopic<String, String> inputForgotPasswordTokenTopic) throws IOException {
        inputForgotPasswordTokenTopic.pipeInput(
                getResource(InputForgotPasswordTokens.VERIFIED_KEY),
                getResource(InputForgotPasswordTokens.VERIFIED_VALUE)
        );
    }

    @Test
    public void testEndToEnd() throws IOException {
        System.out.println("Setup topology driver");
        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9999"); // not existing server
        config.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG,
                org.apache.kafka.streams.errors.LogAndContinueExceptionHandler.class.getName()
        );

        var topology = UserStream.buildTopology().build();
        System.out.println(topology.describe());

        TopologyTestDriver testDriver = new TopologyTestDriver(topology, config);

        var inputUserTopic = testDriver.createInputTopic(DebeziumUsersTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var inputForgotPasswordTokenTopic = testDriver.createInputTopic(DebeziumForgotPasswordTokensTopic.NAME, Serdes.String().serializer(), Serdes.String().serializer());
        var outputVerifiedUsersTopic = testDriver.createOutputTopic(VerifiedUsersTopic.NAME, Serdes.String().deserializer(), Serdes.String().deserializer());
        var outputMailerTopic = testDriver.createOutputTopic(MailerTopic.NAME, Serdes.String().deserializer(), Serdes.String().deserializer());

        //Before testing users lets just double check that forgot password token without user does not do anything.
        testThatForgotPasswordTokenOperationsDoesNothing(
                inputForgotPasswordTokenTopic,
                outputVerifiedUsersTopic,
                outputMailerTopic
        );

        //Testing regular user flow.
        inputUserTopic.pipeInput(
                getResource(InputUsers.UNVERIFIED_KEY),
                getResource(InputUsers.UNVERIFIED_VALUE)
        );

        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());
        var output = outputMailerTopic.readRecord();
        Assertions.assertNull(output.key());
        Assertions.assertEquals(this.getResource(OutputMails.VERIFY_UNVERIFIED_VALUE), output.value());
        Assertions.assertTrue(outputMailerTopic.isEmpty());

        inputUserTopic.pipeInput(
                getResource(InputUsers.VERIFIED_KEY),
                getResource(InputUsers.VERIFIED_VALUE)
        );

        Assertions.assertTrue(outputMailerTopic.isEmpty());
        output = outputVerifiedUsersTopic.readRecord();
        Assertions.assertEquals(this.getResource(OutputUsers.VERIFIED_KEY), output.key());
        Assertions.assertEquals(this.getResource(OutputUsers.VERIFIED_VALUE), output.value());
        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());

        //When we have verified user in KTable lets test the forgot password token flow:
        pipeForgotPasswordVerifiedToken(inputForgotPasswordTokenTopic);
        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());
        output = outputMailerTopic.readRecord();
        Assertions.assertNull(output.key());
        Assertions.assertEquals(getResource(OutputMails.FORGOT_PASSWORD_VERIFIED_VALUE), output.value());
        Assertions.assertTrue(outputMailerTopic.isEmpty());


        testThatForgotPasswordTokenDeleteDoesNothing(
                inputForgotPasswordTokenTopic,
                outputVerifiedUsersTopic,
                outputMailerTopic
        );

        testThatForgotPasswordTokenTombstoneDoesNothing(
                inputForgotPasswordTokenTopic,
                outputVerifiedUsersTopic,
                outputMailerTopic
        );


        //Testing delete
        inputUserTopic.pipeInput(
                getResource(InputUsers.UNVERIFIED_KEY),
                getResource(InputUsers.UNVERIFIED_DELETE_VALUE)
        );

        Assertions.assertTrue(outputMailerTopic.isEmpty());
        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());

        inputUserTopic.pipeInput(
                getResource(InputUsers.UNVERIFIED_KEY),
                "null"
        );

        Assertions.assertTrue(outputMailerTopic.isEmpty());
        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());

        inputUserTopic.pipeInput(
                getResource(InputUsers.VERIFIED_KEY),
                getResource(InputUsers.VERIFIED_DELETE_VALUE)
        );

        Assertions.assertTrue(outputMailerTopic.isEmpty());
        output = outputVerifiedUsersTopic.readRecord();
        Assertions.assertEquals(this.getResource(OutputUsers.VERIFIED_KEY), output.key());
        Assertions.assertNull(output.value());
        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());

        inputUserTopic.pipeInput(
                getResource(InputUsers.VERIFIED_KEY),
                "null"
        );

        Assertions.assertTrue(outputMailerTopic.isEmpty());
        Assertions.assertTrue(outputVerifiedUsersTopic.isEmpty());

        //After the verified user is deleted same should hold true.
        testThatForgotPasswordTokenOperationsDoesNothing(
                inputForgotPasswordTokenTopic,
                outputVerifiedUsersTopic,
                outputMailerTopic
        );

        testDriver.close();
    }

    private String getResource(String resource) throws IOException {
        return Files.readString(Path.of(Objects.requireNonNull(this.getClass().getResource("/messages/" + resource)).getPath()));
    }
}
