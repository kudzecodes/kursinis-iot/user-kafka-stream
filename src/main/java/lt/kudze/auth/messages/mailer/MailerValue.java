package lt.kudze.auth.messages.mailer;

import lt.kudze.auth.messages.user.UserLanguage;

public class MailerValue<PayloadClass> {

    private String from;
    private String to;
    private MailerTemplate template;
    private PayloadClass payload;
    private UserLanguage language;

    public MailerValue() {
        this(null, null, null, null, null);
    }

    public MailerValue(String from, String to, MailerTemplate template, PayloadClass payload, UserLanguage language) {
        this.from = from;
        this.to = to;
        this.template = template;
        this.payload = payload;
        this.language = language;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public MailerTemplate getTemplate() {
        return template;
    }

    public void setTemplate(MailerTemplate template) {
        this.template = template;
    }

    public PayloadClass getPayload() {
        return payload;
    }

    public void setPayload(PayloadClass payload) {
        this.payload = payload;
    }

    public UserLanguage getLanguage() {
        return language;
    }

    public void setLanguage(UserLanguage language) {
        this.language = language;
    }
}
