package lt.kudze.auth.messages.mailer;

import lt.kudze.auth.messages.user.UserLanguage;
import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class ForgotPasswordMailValue extends MailerValue<ForgotPasswordMailPayload> {
    public static final Serde<ForgotPasswordMailValue> serde = JsonSerdes.of(ForgotPasswordMailValue.class);

    public ForgotPasswordMailValue(String from, String to, ForgotPasswordMailPayload payload, UserLanguage language) {
        super(from, to, MailerTemplate.FORGOT_PASSWORD_EMAIL, payload, language);
    }
}
