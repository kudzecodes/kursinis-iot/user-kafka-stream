package lt.kudze.auth.messages.mailer;

public enum MailerTemplate {
    VERIFY_EMAIL,
    FORGOT_PASSWORD_EMAIL
}
