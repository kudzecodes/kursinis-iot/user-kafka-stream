package lt.kudze.auth.messages.mailer;


import com.fasterxml.jackson.annotation.JsonProperty;

public class VerifyMailPayload {
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("verifyLink")
    private String verifyLink;

    public VerifyMailPayload(String firstName, String lastName, String verifyLink) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.verifyLink = verifyLink;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVerifyLink() {
        return verifyLink;
    }

    public void setVerifyLink(String verifyLink) {
        this.verifyLink = verifyLink;
    }
}
