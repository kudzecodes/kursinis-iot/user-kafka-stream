package lt.kudze.auth.messages.mailer;

import lt.kudze.auth.messages.user.UserLanguage;
import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class VerifyMailValue extends MailerValue<VerifyMailPayload> {
    public static final Serde<VerifyMailValue> serde = JsonSerdes.of(VerifyMailValue.class);

    public VerifyMailValue(String from, String to, VerifyMailPayload payload, UserLanguage language) {
        super(from, to, MailerTemplate.VERIFY_EMAIL, payload, language);
    }
}
