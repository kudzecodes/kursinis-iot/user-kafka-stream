package lt.kudze.auth.messages.mailer;


import com.fasterxml.jackson.annotation.JsonProperty;

public class ForgotPasswordMailPayload {
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("submitLink")
    private String submitLink;

    private String code;

    public ForgotPasswordMailPayload(String firstName, String lastName, String verifyLink, String code) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.submitLink = verifyLink;
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSubmitLink() {
        return submitLink;
    }

    public void setSubmitLink(String submitLink) {
        this.submitLink = submitLink;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
