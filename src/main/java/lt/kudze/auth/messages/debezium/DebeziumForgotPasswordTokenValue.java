package lt.kudze.auth.messages.debezium;

import lt.kudze.auth.messages.user.ForgotPasswordTokenValue;
import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumForgotPasswordTokenValue extends DebeziumValue<ForgotPasswordTokenValue> {
    public static final Serde<DebeziumForgotPasswordTokenValue> serde = JsonSerdes.of(DebeziumForgotPasswordTokenValue.class);
}
