package lt.kudze.auth.messages.debezium;

import lt.kudze.auth.messages.user.UserKey;
import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumUserKey extends DebeziumKey<UserKey> {
    public static final Serde<DebeziumUserKey> serde = JsonSerdes.of(DebeziumUserKey.class);
}
