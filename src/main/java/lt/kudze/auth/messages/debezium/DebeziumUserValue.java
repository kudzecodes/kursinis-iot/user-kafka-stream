package lt.kudze.auth.messages.debezium;

import lt.kudze.auth.messages.user.UserValue;
import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumUserValue extends DebeziumValue<UserValue> {
    public static final Serde<DebeziumUserValue> serde = JsonSerdes.of(DebeziumUserValue.class);
}
