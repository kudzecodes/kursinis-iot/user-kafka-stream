package lt.kudze.auth.messages.debezium;

import lt.kudze.auth.messages.user.ForgotPasswordTokenKey;
import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class DebeziumForgotPasswordTokenKey extends DebeziumKey<ForgotPasswordTokenKey> {
    public static final Serde<DebeziumForgotPasswordTokenKey> serde = JsonSerdes.of(DebeziumForgotPasswordTokenKey.class);
}
