package lt.kudze.auth.messages.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public enum UserLanguage {
    @JsonProperty("lt")
    LITHUANIAN,
    @JsonProperty("en")
    ENGLISH;

    @JsonValue
    public String getValue() {
        switch (this) {
            case LITHUANIAN:
                return "lt";
            case ENGLISH:
                return "en";
            default:
                return null;
        }
    }

}
