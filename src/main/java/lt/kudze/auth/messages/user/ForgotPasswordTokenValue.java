package lt.kudze.auth.messages.user;

import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class ForgotPasswordTokenValue {
    public static final Serde<ForgotPasswordTokenValue> serde = JsonSerdes.of(ForgotPasswordTokenValue.class);

    private String userUuid;
    private String token;
    private String createdAt;

    public ForgotPasswordTokenValue() {

    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
