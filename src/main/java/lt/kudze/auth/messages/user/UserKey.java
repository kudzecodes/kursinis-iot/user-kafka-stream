package lt.kudze.auth.messages.user;

import lt.kudze.auth.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class UserKey {
    public static final Serde<UserKey> serde = StringSerdes.from(
            UserKey::toString,
            UserKey::new
    );

    private String uuid;

    public UserKey() {
        this(null);
    }

    public UserKey(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return this.uuid;
    }
}
