package lt.kudze.auth.messages.user;

import lt.kudze.auth.serdes.StringSerdes;
import org.apache.kafka.common.serialization.Serde;

public class ForgotPasswordTokenKey {
    public static final Serde<ForgotPasswordTokenKey> serde = StringSerdes.from(
            ForgotPasswordTokenKey::toString,
            ForgotPasswordTokenKey::new
    );

    private String userUuid;

    public ForgotPasswordTokenKey() {
        this(null);
    }

    public ForgotPasswordTokenKey(String userUuid) {
        this.userUuid = userUuid;
    }

    public String getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(String userUuid) {
        this.userUuid = userUuid;
    }

    @Override
    public String toString() {
        return this.userUuid;
    }
}
