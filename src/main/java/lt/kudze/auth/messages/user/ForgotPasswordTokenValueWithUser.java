package lt.kudze.auth.messages.user;

import lt.kudze.auth.serdes.JsonSerdes;
import org.apache.kafka.common.serialization.Serde;

public class ForgotPasswordTokenValueWithUser {
    public static final Serde<ForgotPasswordTokenValueWithUser> serde = JsonSerdes.of(ForgotPasswordTokenValueWithUser.class);

    private ForgotPasswordTokenValue token;
    private UserValue user;

    public ForgotPasswordTokenValueWithUser() {

    }

    public ForgotPasswordTokenValueWithUser(ForgotPasswordTokenValue token, UserValue user) {
        this.token = token;
        this.user = user;
    }

    public ForgotPasswordTokenValue getToken() {
        return token;
    }

    public UserValue getUser() {
        return user;
    }
}
