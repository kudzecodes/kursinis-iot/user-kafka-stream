package lt.kudze.auth.topics;

import lt.kudze.auth.messages.debezium.DebeziumForgotPasswordTokenKey;
import lt.kudze.auth.messages.debezium.DebeziumForgotPasswordTokenValue;
import lt.kudze.auth.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DebeziumForgotPasswordTokensTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEBEZIUM_FORGOT_PASSWORD_TOKEN_TOPIC");
    public static final Consumed<DebeziumForgotPasswordTokenKey, DebeziumForgotPasswordTokenValue> CONSUMED = Consumed.with(DebeziumForgotPasswordTokenKey.serde, DebeziumForgotPasswordTokenValue.serde);
}
