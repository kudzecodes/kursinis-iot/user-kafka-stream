package lt.kudze.auth.topics;

import lt.kudze.auth.messages.user.UserKey;
import lt.kudze.auth.messages.user.UserValue;
import lt.kudze.auth.util.SystemUtil;
import org.apache.kafka.streams.kstream.Produced;

public class VerifiedUsersTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_VERIFIED_USERS_TOPIC");
    public static final Produced<UserKey, UserValue> PRODUCED = Produced.with(UserKey.serde, UserValue.serde);
}
