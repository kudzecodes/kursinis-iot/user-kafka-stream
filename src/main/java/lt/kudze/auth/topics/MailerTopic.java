package lt.kudze.auth.topics;

import lt.kudze.auth.messages.mailer.ForgotPasswordMailValue;
import lt.kudze.auth.messages.mailer.VerifyMailValue;
import lt.kudze.auth.serdes.NullSerdes;
import lt.kudze.auth.util.SystemUtil;
import org.apache.kafka.streams.kstream.Produced;

public class MailerTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_MAILER_TOPIC");
    public static final Produced<NullSerdes, VerifyMailValue> PRODUCED_VERIFY = Produced.with(NullSerdes.serde, VerifyMailValue.serde);
    public static final Produced<NullSerdes, ForgotPasswordMailValue> PRODUCED_FORGOT_PASSWORD = Produced.with(NullSerdes.serde, ForgotPasswordMailValue.serde);
}
