package lt.kudze.auth.topics;

import lt.kudze.auth.messages.debezium.DebeziumUserKey;
import lt.kudze.auth.messages.debezium.DebeziumUserValue;
import lt.kudze.auth.util.SystemUtil;
import org.apache.kafka.streams.kstream.Consumed;

public class DebeziumUsersTopic {
    public static final String NAME = SystemUtil.getenv("KAFKA_DEBEZIUM_USERS_TOPIC");
    public static final Consumed<DebeziumUserKey, DebeziumUserValue> CONSUMED = Consumed.with(DebeziumUserKey.serde, DebeziumUserValue.serde);
}
