/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package lt.kudze.auth;

import lt.kudze.auth.messages.debezium.DebeziumUserKey;
import lt.kudze.auth.messages.debezium.DebeziumUserValue;
import lt.kudze.auth.messages.debezium.DebeziumValue;
import lt.kudze.auth.messages.debezium.DebeziumValuePayload;
import lt.kudze.auth.messages.mailer.ForgotPasswordMailPayload;
import lt.kudze.auth.messages.mailer.ForgotPasswordMailValue;
import lt.kudze.auth.messages.mailer.VerifyMailPayload;
import lt.kudze.auth.messages.mailer.VerifyMailValue;
import lt.kudze.auth.messages.user.ForgotPasswordTokenValue;
import lt.kudze.auth.messages.user.ForgotPasswordTokenValueWithUser;
import lt.kudze.auth.messages.user.UserKey;
import lt.kudze.auth.messages.user.UserValue;
import lt.kudze.auth.serdes.NullSerdes;
import lt.kudze.auth.topics.DebeziumForgotPasswordTokensTopic;
import lt.kudze.auth.topics.DebeziumUsersTopic;
import lt.kudze.auth.topics.MailerTopic;
import lt.kudze.auth.topics.VerifiedUsersTopic;
import lt.kudze.auth.util.ForgotPasswordLinkFactory;
import lt.kudze.auth.util.SystemUtil;
import lt.kudze.auth.util.VerifyLinkFactory;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;

import java.net.URISyntaxException;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

/**
 * In this example, we implement a simple WordCount program using the high-level Streams DSL
 * that reads from a source topic "streams-plaintext-input", where the values of messages represent lines of text,
 * split each text line into words and then compute the word occurrence histogram, write the continuous updated histogram
 * into a topic "streams-wordcount-output" where each record is an updated count of a single word.
 */
public class UserStream {

    public static boolean isPayloadFromVerifiedUser(DebeziumValuePayload<UserValue> payload) {
        return (payload.getAfter().isEmpty() && payload.getBefore().filter(UserValue::isVerified).isPresent()) || payload.getAfter().filter(UserValue::isVerified).isPresent();
    }

    public static KTable<UserKey, UserValue> buildUserTopology(StreamsBuilder builder) {
        KStream<DebeziumUserKey, DebeziumUserValue> debeziumUsersStream = builder.stream(DebeziumUsersTopic.NAME, DebeziumUsersTopic.CONSUMED);

        var userPayloadStream = debeziumUsersStream.filter(
                (key, value) -> value != null,
                Named.as("users-filter-tombstone")
        ).mapValues(
                DebeziumValue::getPayload,
                Named.as("users-map-payload")
        );

        var verifiedUsersStream = userPayloadStream.filter(
                (key, value) -> isPayloadFromVerifiedUser(value),
                Named.as("users-filter-verified")
        ).map(
                (key, value) -> new KeyValue<>(key.getPayload(), value.getAfter().orElse(null)),
                Named.as("users-verified-map-after")
        );
        verifiedUsersStream.to(VerifiedUsersTopic.NAME, VerifiedUsersTopic.PRODUCED);

        buildUserVerifyEmailTopology(userPayloadStream);

        return verifiedUsersStream.toTable(
                Named.as("users-verified-to-table"),
                Materialized.<UserKey, UserValue, KeyValueStore<Bytes, byte[]>>as("users-verified-table")
                        .withKeySerde(UserKey.serde)
                        .withValueSerde(UserValue.serde)
        );
    }

    public static void buildUserVerifyEmailTopology(KStream<DebeziumUserKey, DebeziumValuePayload<UserValue>> userPayloadStream) {
        KStream<NullSerdes, VerifyMailValue> userVerifyEmailStream = userPayloadStream.filter(
                (key, value) -> !isPayloadFromVerifiedUser(value),
                Named.as("users-filter-unverified")
        ).map(
                (key, value) -> new KeyValue<>(null, value.getAfter().orElse(null)),
                Named.as("users-unverified-map-after")
        ).filter(
                (key, value) -> value != null,
                Named.as("users-unverified-filter-undeleted")
        ).map(
                (key, value) -> {
                    try {
                        return new KeyValue<>(null, new VerifyMailValue(
                                SystemUtil.getenv("MAILER_FROM"),
                                value.getEmail(),
                                new VerifyMailPayload(
                                        value.getFirstName(),
                                        value.getLastName(),
                                        VerifyLinkFactory.buildVerifyLink(value)
                                ),
                                value.getLanguage()
                        ));
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                },
                Named.as("users-verify-email-map")
        );
        userVerifyEmailStream.to(MailerTopic.NAME, MailerTopic.PRODUCED_VERIFY);
    }

    public static void buildForgotPasswordEmailTopology(StreamsBuilder builder, KTable<UserKey, UserValue> verifiedUserTable) {
        var debeziumForgotPasswordTokensStream = builder.stream(DebeziumForgotPasswordTokensTopic.NAME, DebeziumForgotPasswordTokensTopic.CONSUMED);

        KStream<NullSerdes, ForgotPasswordMailValue> forgotPasswordMailStream = debeziumForgotPasswordTokensStream.filter(
                (key, value) -> value != null,
                Named.as("forgot-password-tokens-filter-tombstone")
        ).mapValues(
                DebeziumValue::getPayload,
                Named.as("forgot-password-tokens-map-payload")
        ).mapValues(
                (value) -> value.getAfter().orElse(null),
                Named.as("forgot-password-tokens-map-after")
        ).filter(
                (key, value) -> value != null,
                Named.as("forgot-password-tokens-filter-undeleted")
        ).map(
                (key, value) -> new KeyValue<>(new UserKey(value.getUserUuid()), value),
                Named.as("forgot-password-tokens-key-to-users")
        ).join(
                verifiedUserTable,
                ForgotPasswordTokenValueWithUser::new,
                Joined.with(
                        UserKey.serde, ForgotPasswordTokenValue.serde, UserValue.serde,
                        "forgot-password-tokens-join-verified-users"
                )
        ).map(
                (key, value) -> {
                    var user = value.getUser();
                    var token = value.getToken();

                    try {
                        return new KeyValue<>(null, new ForgotPasswordMailValue(
                                SystemUtil.getenv("MAILER_FROM"),
                                user.getEmail(),
                                new ForgotPasswordMailPayload(
                                        user.getFirstName(),
                                        user.getLastName(),
                                        ForgotPasswordLinkFactory.buildVerifyLink(token, user),
                                        token.getToken()
                                ),
                                user.getLanguage()
                        ));
                    } catch (URISyntaxException e) {
                        throw new RuntimeException(e);
                    }
                },
                Named.as("users-forgot-password-email-map")
        );
        forgotPasswordMailStream.to(MailerTopic.NAME, MailerTopic.PRODUCED_FORGOT_PASSWORD);
    }

    public static StreamsBuilder buildTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        var usersTable = buildUserTopology(builder);
        buildForgotPasswordEmailTopology(builder, usersTable);

        return builder;
    }

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, SystemUtil.getenv("KAFKA_GROUP_ID"));
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, SystemUtil.getenv("KAFKA_BROKERS"));
        props.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, SystemUtil.getenv("KAFKA_SECURITY_PROTOCOL", "PLAINTEXT"));
        props.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_LOCATION", null));
        props.put(SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, SystemUtil.getenv("KAFKA_TRUSTSTORE_PASSWORD", null));
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        final StreamsBuilder builder = UserStream.buildTopology();

        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
