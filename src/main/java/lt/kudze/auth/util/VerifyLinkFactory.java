package lt.kudze.auth.util;

import lt.kudze.auth.messages.user.UserLanguage;
import lt.kudze.auth.messages.user.UserValue;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;

public class VerifyLinkFactory {

    public static String buildVerifyLink(UserValue userValue) throws URISyntaxException {
        if (userValue.isVerified())
            throw new RuntimeException("Cannot build verify link for already verified user!");

        URIBuilder url = new URIBuilder(SystemUtil.getenv("AUTH_VERIFY_URL", "http://localhost/verify"));
        url.addParameter("code", userValue.getEmailVerificationCode());
        url.addParameter("email", userValue.getEmail());
        url.addParameter("lang", userValue.getLanguage().getValue());

        return url.build().toString();
    }

}
