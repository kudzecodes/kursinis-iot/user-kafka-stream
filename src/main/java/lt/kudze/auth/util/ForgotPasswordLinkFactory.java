package lt.kudze.auth.util;

import lt.kudze.auth.messages.user.ForgotPasswordTokenValue;
import lt.kudze.auth.messages.user.UserValue;
import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;

public class ForgotPasswordLinkFactory {

    public static String buildVerifyLink(ForgotPasswordTokenValue token, UserValue user) throws URISyntaxException {
        URIBuilder url = new URIBuilder(SystemUtil.getenv("AUTH_FORGOT_PASSWORD_URL", "http://localhost/password/forgot"));
        url.addParameter("code", token.getToken());
        url.addParameter("email", user.getEmail());
        url.addParameter("lang", user.getLanguage().getValue());

        return url.build().toString();
    }

}
