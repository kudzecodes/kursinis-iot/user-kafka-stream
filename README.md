# User Kafka Stream

This is a simple kafka streams application that manages users data
provided by MySQL debezium source connector reading from https://auth.kudze.lt backend.

## Specification

Debezium is putting all users to `user-auth.user_auth.users` topic.

Debezium is putting all forgot password tokens to `user-auth.user_auth.forgot_password_tokens` topic.

This kafka stream application filters out verified users to `user-auth.user_auth.verified-users` topic.

This kafka stream application also produces a message to `mailer` topic for email verification for unverified users.

This kafka stream application also produces a message to `mailer` topic for forgot password feature for unverified users.